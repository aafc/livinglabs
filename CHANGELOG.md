# Changelog

All notable changes to this project will be added in this file. Changes are released together in a specified build. Tags (for the builds) are created after a new entry in this file is added.

## 1.00.05 - 2020-04-29
### Added

- 323: Text updated to terms of use
- Merged TOS-text-update branch
- Changed text and added custom modal for Terms of Servcie

## 1.00.04 - 2020-03-24
### Added

- 304: Nextcloud: CSV file behaves inconsistently

## 1.00.03 - 2020-03-18

### Added

- Number of changes for WCAG
- Fixed TOS bypass bug (on internal links)
- Updated chrome download script because txt and md files no longer have previews

## 1.00.02 - 2020-03-10

### Added

- Strictly fixes to WCAG
- Mostly changes to contrast (i.e. between text and background) and focus indicators

## 1.00.01 - 2020-03-02

### Added

- Fixed drop-down menu for assigning users to groups
- Fixed Chrome popup on files that allow previews

## 1.00.00 - 2020-02-24

### Added

- Create popup when downloading in Chrome
- Remove creating public links for certain groups
- Terms of service popup

