# Configurations

This file contains any configurations that are needed for the Living Labs Nextcloud instance. These must be configured once the instance has been setup, by the system admin. 

# Plugins

## Living Labs

Version: 0.0.1

Description: A Nextcloud plugin used to implement features for the Living Labs project

Installation: See README.md for installation instructions


