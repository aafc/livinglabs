// A javascript file containing smaller scripts that don't necessitate a seperate file

/**
 * Fixed for drop-down overlay issues in index.php/settings/users
 * Reported here: https://github.com/nextcloud/server/pull/19533
 */

$(document).on('click', ".toggleUserActions", function() {
    $('#app-content div.row--editable').css({'z-index': 'auto'})
})