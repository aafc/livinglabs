var confirmMsg = "Proceed with download?"
var confirmMsgfr = "Procéder au téléchagement?"

var isChrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());

// Exclude popup for files that will be previewed (instead of directly downloaded) in Nextcloud
function excludeFiles(datamime) {
    const supported_types = ['image/jpeg', 'image/png', 'image/bmp', 'image/gif', 'video/mp4', 'text/csv', 'text/plain', 'text/markdown', 'application/pdf']
    for (var i = 0; i < supported_types.length; i++) {
        if (supported_types[i] === datamime) { return true; }
    }
    return false;
}

if (isChrome) {
    $(document).ready(function(){
        // Popup when clicking file bar
        $("#fileList").on('click', 'a .nametext', function(){
            var parent = $(this).closest('tr')
            var datatype = parent.attr('data-type')             // datatype == dir or file ?
            var datamime =  parent.attr('data-mime')            // datamime == image/jpeg, image/png, ... ?
            //Bug 355: Don't bother with download prompt...It will be based on size
            if (datatype === 'file' && !excludeFiles(datamime)) { //} && !window.confirm(confirmMsg)) {
                event.preventDefault();
            }
        })

        // Popup when clicking file thumbnail
        $("#fileList").on('click', 'a .thumbnail-wrapper', function(){
            var parent = $(this).closest('tr')
            var datatype = parent.attr('data-type')
            var datamime =  parent.attr('data-mime')
            //Bug 355: Don't bother with download prompt...It will be based on size
            if (datatype === 'file' && !excludeFiles(datamime)){ //} && !window.confirm(confirmMsg)) {
                event.preventDefault();
            }
        })
    })
}